package solarvolt.carlookup;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Size;
import android.util.SparseArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {
    private static final int PERMISSIONS_REQUEST = 721;
    private static final int FILE_REQUEST_CODE = 831;

    public static final String PREFS_NAME = "Prefs";

    private Uri dbUri = null;
    private TextureView textureView;
    private LinearLayout bottomSheet;
    private BottomSheetBehavior bottomSheetBehavior;
    private String cameraId;
    private CameraDevice cameraDevice;
    private CameraManager cameraManager;
    private CameraCaptureSession cameraCaptureSession;
    private CaptureRequest.Builder captureRequestBuilder;
    private CaptureRequest captureRequest;
    private Size previewSize;
    private HandlerThread backgroundThread = null;
    private Handler backgroundHandler = null;
    private TextRecognizer textRecognizer;
    private TextureView.SurfaceTextureListener surfaceTextureListener;
    private SQLiteDatabase db = null;

    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice cameraDevice) {
            Log.v(null, "Camera Opened");
            MainActivity.this.cameraDevice = cameraDevice;
            createCameraPreview();
        }
        @Override
        public void onDisconnected(CameraDevice cameraDevice) {
            Log.v(null, "Camera Disconnected");
            cameraDevice.close();
            MainActivity.this.cameraDevice = null;
        }
        @Override
        public void onError(CameraDevice cameraDevice, int error) {
            Log.v(null, "Camera Error");
            cameraDevice.close();
            MainActivity.this.cameraDevice = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

        String lastDBUriStr = settings.getString("DBUri", "");
        if (lastDBUriStr.isEmpty()) {
            Toast.makeText(this, "No database selected", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Using Database: " + lastDBUriStr, Toast.LENGTH_SHORT).show();
            dbUri = Uri.parse(lastDBUriStr);
            openDBFromUri(dbUri);
        }

        setContentView(R.layout.activity_main);

        permissionCheck();

        textureView = (TextureView) findViewById(R.id.texture_view);
        bottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet);

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        FloatingActionButton fab_find = (FloatingActionButton) findViewById(R.id.fab_find);
        fab_find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickFind();
            }
        });
        FloatingActionButton fab_file = (FloatingActionButton) findViewById(R.id.fab_file);
        fab_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDBFile();
            }
        });

        cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        surfaceTextureListener = new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                setUpCamera();
                openCamera();
            }
            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            }
            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                return false;
            }
            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {
            }
        };

        textRecognizer = new TextRecognizer.Builder(this).build();
        if (!textRecognizer.isOperational()) {
            throw new RuntimeException("GMS Service Error");
        }
    }


    @Override
    protected void onActivityResult (int requestCode,
                                     int resultCode,
                                     Intent data)
    {
        if ((requestCode == FILE_REQUEST_CODE) && (resultCode == RESULT_OK))
        {
            dbUri = data.getData();
            Log.i(null, "Using URI: " + dbUri);
            openDBFromUri(dbUri);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        openBackgroundThread();
        if (textureView.isAvailable()) {
            setUpCamera();
            openCamera();
        } else {
            textureView.setSurfaceTextureListener(surfaceTextureListener);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("DBUri", dbUri == null? "" : dbUri.toString());
        editor.commit();
    }

    @Override
    protected void onStop() {
        super.onStop();
        closeCamera();
        closeBackgroundThread();
    }

    @Override
    public void onBackPressed() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            unfreezeCamera();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(null, "Permission grant");
                } else {
                    finish();
                }
                return;
            }
        }
    }

    private void permissionCheck() {
        String[] Permissions = {
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE
        };
        requestPermissions(Permissions, PERMISSIONS_REQUEST);

    }

    private void closeCamera() {
        if (cameraCaptureSession != null) {
            cameraCaptureSession.close();
            cameraCaptureSession = null;
        }

        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }
    }

    private void closeBackgroundThread() {
        if (backgroundHandler != null) {
            backgroundThread.quitSafely();
            backgroundThread = null;
            backgroundHandler = null;
        }
    }

    private void setUpCamera() {
        try {
            for (String cameraId : cameraManager.getCameraIdList()) {
                CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);
                if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_BACK)
                {
                    StreamConfigurationMap streamConfigurationMap = cameraCharacteristics.get(
                            CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                    previewSize = streamConfigurationMap.getOutputSizes(SurfaceTexture.class)[0];
                    this.cameraId = cameraId;
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void openCamera() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                cameraManager.openCamera(cameraId, stateCallback, null);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void openBackgroundThread() {
        backgroundThread = new HandlerThread("camera_background_thread");
        backgroundThread.start();
        backgroundHandler = new Handler(backgroundThread.getLooper());
    }

    protected void createCameraPreview() {
        try {
            SurfaceTexture surfaceTexture = textureView.getSurfaceTexture();
            surfaceTexture.setDefaultBufferSize(previewSize.getWidth(), previewSize.getHeight());
            Surface previewSurface = new Surface(surfaceTexture);
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(previewSurface);

            cameraDevice.createCaptureSession(Collections.singletonList(previewSurface),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                            if (cameraDevice == null) {
                                return;
                            }

                            try {
                                captureRequest = captureRequestBuilder.build();
                                MainActivity.this.cameraCaptureSession = cameraCaptureSession;
                                MainActivity.this.cameraCaptureSession.setRepeatingRequest(captureRequest,
                                        null, backgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
                        }
                    }, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void onClickFind() {
        freezeCamera();

        Bitmap textBitmap = textureView.getBitmap();
        Frame frame = new Frame.Builder().setBitmap(textBitmap).build();

        String carId = detectCarId(frame);

        if (carId != null) {
            lookupCarIdFromDB(carId);
        } else {
            Log.i(null, "No CarId detected");
            Toast.makeText(this, "No CarId detected", Toast.LENGTH_SHORT).show();
            unfreezeCamera();
        }

    }

    private void popupNotFound(String id) {
        Log.i(null, "Not found from DB: " + id);
        popupInfo(id, "Not found from DB", "", "", "");
    }

    private void popupInfo(String id, String info1, String info2, String info3, String info4) {
        TextView textCarId = (TextView) findViewById(R.id.text_car_id);
        TextView textInfo1 = (TextView) findViewById(R.id.text_car_info1);
        TextView textInfo2 = (TextView) findViewById(R.id.text_car_info2);
        TextView textInfo3 = (TextView) findViewById(R.id.text_car_info3);
        TextView textInfo4 = (TextView) findViewById(R.id.text_car_info4);

        textCarId.setText(id);
        textInfo1.setText(info1);
        textInfo2.setText(info2);
        textInfo3.setText(info3);
        textInfo4.setText(info4);

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private void selectDBFile() {
        Intent intent = new Intent()
                .setType("*/*")
                .setAction(Intent.ACTION_OPEN_DOCUMENT);

        startActivityForResult(Intent.createChooser(intent, "Select a file"), FILE_REQUEST_CODE);

    }

    private void openDBFromUri(Uri dbUri) {
        try {
            Toast.makeText(this, "Using Database: " + dbUri.toString(), Toast.LENGTH_SHORT).show();
            File file = null;
            InputStream inputStream = getContentResolver().openInputStream(dbUri);
            file = File.createTempFile("cardb", "sqlite");

            FileOutputStream outputStream = new FileOutputStream(file);
            byte[] buff = new byte[1024];
            int read;
            while ((read = inputStream.read(buff, 0, buff.length)) > 0)
                outputStream.write(buff, 0, read);
            inputStream.close();
            outputStream.close();

            db = SQLiteDatabase.openOrCreateDatabase(file.getPath(), null);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void lookupCarIdFromDB(String carId) {
        if (db == null) {
            Toast.makeText(this, "No database", Toast.LENGTH_SHORT).show();
            unfreezeCamera();
            return;
        }

        Log.i(null, "Looking into database for " + carId);

        String TABLE_NAME = "Car";
        try {
            Cursor cursor = db.rawQuery("select id, info1, info2, info3, info4 from " + TABLE_NAME + " where id = \"" + carId + "\"", null);
            if (cursor.getCount() == 1) {
                Log.i(null, "Found 1 record");
                cursor.moveToNext();
                String info1 = cursor.getString(cursor.getColumnIndex("info1"));
                String info2 = cursor.getString(cursor.getColumnIndex("info2"));
                String info3 = cursor.getString(cursor.getColumnIndex("info3"));
                String info4 = cursor.getString(cursor.getColumnIndex("info4"));
                popupInfo(carId, info1, info2, info3, info4);
            } else {
                popupNotFound(carId);
            }
            cursor.close();
        } catch (SQLiteException e) {
            Log.w(null, e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            unfreezeCamera();
        }

    }

    private String detectCarId(Frame frame) {
        String carId = null;
        SparseArray<TextBlock> textBlocks = textRecognizer.detect(frame);
        if (textBlocks.size() == 0) return null;
        // Process only the first text block
        TextBlock item = textBlocks.valueAt(0);
        if (item != null && item.getValue() != null) {
            String detectedText = item.getValue();
            detectedText = detectedText.replaceAll("\\s+","");

            String tradCarIdPattern = "[A-Z][A-Z][0-9]{1,4}";
            String tradCarIdPrefixPattern = "[A-Z][A-Z]";
            if (detectedText.matches(tradCarIdPattern)) {
                Log.i(null, "Detected: " + detectedText);
                carId = detectedText;
            } else if (detectedText.matches(tradCarIdPrefixPattern)) {
                TextBlock nextItem = textBlocks.valueAt(1);
                if (nextItem != null && nextItem.getValue() != null) {
                    detectedText.concat(nextItem.getValue());
                }
                detectedText = detectedText.replaceAll("\\s+","");
                if (detectedText.matches(tradCarIdPattern)) {
                    Log.i(null, "Detected: " + detectedText);
                    carId = detectedText;
                }
            }
        }

        return carId;
    }

    private void freezeCamera() {
        try {
            cameraCaptureSession.stopRepeating();
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void unfreezeCamera() {
        try {
            cameraCaptureSession.setRepeatingRequest(captureRequestBuilder.build(),
                    null, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }
}
